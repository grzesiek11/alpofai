#!/bin/sh

. "$(readlink -f "$(dirname "$0")")/common"

# Make a backup of save files
rm -rf "$backup_dir/User"
cp -r "$dist_dir/User" "$backup_dir/User" 2>/dev/null && restore_backup=1

rm -rf "$dist_dir" "$temp_dir"
mkdir "$dist_dir" "$temp_dir"

# Restore the save backup
[ "$restore_backup" = 1 ] && cp -r "$backup_dir/User" "$dist_dir/User"

# Copy game data from macOS ADOFAI
cp -r "$req_dir/adofai/ADanceOfFireAndIce.app/Contents/Resources/Data" "$dist_dir/${game_name}_Data" || game_integrity_fail=1
cp -r "$req_dir/adofai/ADanceOfFireAndIce.app/Contents/Resources/unity default resources" "$dist_dir/${game_name}_Data/Resources/unity default resources" || game_integrity_fail=1

[ "$game_integrity_fail" = 1 ] && { printf '[ERROR] The game download is incomplete. Did download_requirements complete successfully?\n' && exit 1; }

# Extract and copy runtime files from the Unity runtime
mkdir "$temp_dir/unity_runtime"
(
    cd "$temp_dir/unity_runtime" && 7z x "$req_dir/unity_runtime.exe" || runtime_integrity_fail=1
)

cp "$temp_dir/unity_runtime/\$INSTDIR\$_"??"_/Variations/linux64_withgfx_nondevelopment_mono/LinuxPlayer" "$dist_dir/$game_name" && chmod a+x "$dist_dir/$game_name" || runtime_integrity_fail=1
cp "$temp_dir/unity_runtime/\$INSTDIR\$_"??"_/Variations/linux64_withgfx_nondevelopment_mono/UnityPlayer.so" "$dist_dir/UnityPlayer.so" || runtime_integrity_fail=1
cp -r "$temp_dir/unity_runtime/\$INSTDIR\$_"??"_/Variations/linux64_withgfx_nondevelopment_mono/Data/MonoBleedingEdge" "$dist_dir/${game_name}_Data/MonoBleedingEdge" || runtime_integrity_fail=1

[ "$runtime_integrity_fail" = 1 ] && { printf '[ERROR] The Unity runtime download is incomplete. Did download_requirements complete successfully?\n' && exit 1; }

# Extract and copy required libraries
mkdir -p "$dist_dir/${game_name}_Data/Plugins"
# libdiscord-rpc.so
unzip -d "$temp_dir/discord_rpc" "$req_dir/discord_rpc.zip" || plugin_integrity_fail=1
cp "$temp_dir/discord_rpc/discord-rpc/linux-dynamic/lib/libdiscord-rpc.so" "$dist_dir/${game_name}_Data/Plugins/libdiscord-rpc.so" || plugin_integrity_fail=1
# libsteam_api.so
cp "$req_dir/libsteam_api.so" "$dist_dir/${game_name}_Data/Plugins/libsteam_api.so" || plugin_integrity_fail=1
# libStandaloneFileBrowser.so
cp "$req_dir/libStandaloneFileBrowser.so" "$dist_dir/${game_name}_Data/Plugins/libStandaloneFileBrowser.so" || plugin_integrity_fail=1

[ "$plugin_integrity_fail" = 1 ] && { printf '[ERROR] Plugin download is incomplete. Did download_requirements complete successfully?\n' && exit 1; }

# Set AppID for launching the game directly with working Steam API
printf '%s' "$steam_appid" > "$dist_dir/steam_appid.txt"

# Generate a desktop file
cp "$res_dir/icon.png" "$dist_dir/icon.png"
cat > "$dist_dir/$desktop_file" << EOF
[Desktop Entry]
Type=Application
Name=$desktop_name
GenericName=$desktop_desc
Path=$dist_dir
Exec=$dist_dir/$game_name
Icon=$dist_dir/icon.png
Categories=Game;
EOF
chmod +x "$dist_dir/$desktop_file"
